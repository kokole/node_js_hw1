const http = require('http');
const fs = require('fs').promises;
const Fs = require('fs');
const express = require('express');
const app = express();
const host = 'localhost';
const port = 8080;
const path = require('path');
const morgan = require('morgan');
const bodyParser = require("body-parser");

app.use(express.json());
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let dir = './Notes'
if (!Fs.existsSync(dir)){
    Fs.mkdirSync(dir);
}

app.post('/api/files', function(req, res) {
  if (req.body.filename && req.body.content) {
    let ext = path.extname(req.body.filename);
    const extList = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
    if (extList.includes(ext)) {
      fs.writeFile(`./Notes/${req.body.filename}`, req.body.content, function(err){
         if(err) {
           res.status(400).send({message: 'Error message'});
        };
       })
       res.status(200);
      res.send({message: 'File created successfully'});
    } else {
      res.status(400);
      return res.send({message: 'Error message'});
    }
  } else {
    res.status(400).send({message: 'Please specify "content" parameter'});
  };
});

app.get('/api/files/:filename', function(req, res) {
  if (!req.params.filename) {
    res.status(400).send({message: 'Error message'});
    return;
  }
  let qdata = req.params.filename;
  let ext = path.extname(qdata);
  
  fs.readFile(`./Notes/${qdata}`, {encoding:'utf8'})
		.then(data => {
      const { birthtime } = Fs.statSync(`./Notes/${qdata}`);
			res.status(200).send({
        message: "Success",
        filename: req.params.filename,
        content: data,
        extension: ext.slice(1),
        uploadedDate: birthtime
      })
		})
		.catch(err => {
			res.status(400);
			return res.send({message: 'Error message'});
		});
})

app.get('/api/files', function(req, res) {
	fs.readdir('./Notes')
		.then(files=>{
			res.status(200).json({
        message: 'Success',
        files: files
      })	
		})
    .catch(err => {
      res.status(400);
      return res.send({message: 'Error message'});
    })		
})

app.delete('/api/files/:filename', function(req, res) {
	const file = `./Notes/${req.params.filename}`
	fs.unlink(file)
		.then(()=>{
			res.status(200).send({message: 'Successfully deleted the file.'});
		})
		.catch(err=>{
			res.status(400);
			return res.send({message: 'Error message'});
		})
})

app.listen(port, function(err){
    if (err) console.log(err);
    console.log("Server listening on PORT", port);
});
